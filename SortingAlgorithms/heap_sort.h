﻿#pragma once

#include <vector>
#include "sort.h"

template<class T>
class heap_sort
{
public:
	static std::vector<T> sort(const std::vector<T>& data, sorting_order order = Ascending)
	{
		std::vector<T> tmp_data = data;
		sort_(tmp_data, order);
		return tmp_data;
	}

private:
	static bool compare(T left, T right, sorting_order order)
	{
		if (order == Ascending)
			return left > right;
		else
			return left < right;
	}

	static void make_heap(std::vector<T>& data, int n, int i, sorting_order order)
	{
		int largest = i;
		int l = 2 * i + 1;
		int r = 2 * i + 2;

		if (l<n && compare(data[l], data[largest], order))
			largest = l;
		if (r < n && compare(data[r], data[largest], order))
			largest = r;
		if(largest!=i)
		{
			std::swap(data[i], data[largest]);

			make_heap(data, n, largest, order);
		}
	}

	static void sort_(std::vector<T>& data, sorting_order order)
	{
		for(int i=data.size()/2 - 1; i>=0 ;i--)
		{
			make_heap(data, data.size(), i, order);
		}
		for( int i=data.size()-1; i>=0 ;i--)
		{
			std::swap(data[0], data[i]);
			make_heap(data, i, 0, order);
		}
	}

};