#include <vector>
#include <iostream>
#include <chrono>
#include <fstream>
#include <locale>
#include <iomanip>

#include "algorithms_testing.h"
#include "quick_sort.h"
#include "merge_sort.h"
#include "intro_sort.h"

int main()
{
	const std::locale locale("polish");
	std::locale::global(locale);
	std::cout.imbue(locale);

	const int arrays_number = 100;
	std::vector<int> sizes = {1000, 2500, 5000, 7500, 10000, 25000, 50000, 75000, 100000, 250000, 500000, 750000, 1000000, 1500000 };
	std::vector<double> partials = {0.0, 0.25, 0.5, 0.75, 0.9, 0.95, 0.99, 0.997, -1.0};

	std::ofstream file;
	file.open(R"(C:/pamsi/p2/results.csv)", std::fstream::out | std::fstream::trunc);
	if (!file.is_open())
		return -1;

	quick_sort<int> quick_sort{};
	intro_sort<int> intro_sort{};
	merge_sort<int> merge_sort{};

	test_result sort_result;

	for (auto partial : partials)
	{
		std::cout << "Partially sorted: " << partial * 100 << " %\n";
		std::cout << std::setw(14) << "Size: " << std::setw(13) << "quick sort: ";
		std::cout << std::setw(13) << "intro sort: " << std::setw(13) << "merge sort: \n";

		file << "Partially sorted: " << partial * 100 << " %\n";
		file << "Size;quick sort;intro sort;merge sort\n";

		for (auto size : sizes)
		{
			std::vector<std::vector<int>> arrays;
			arrays.reserve(arrays_number);

			for (int i = 0; i < arrays_number; i++)
			{
				arrays.push_back(generate_test_array(size, partial));
			}

			std::cout << std::setw(12) << size << ";";			

			test_sort(arrays, sort_result, quick_sort);
			const auto quick_sort_time = sort_result.duration.count() / sort_result.arrays.size();
			std::cout << std::setw(12) << quick_sort_time << " s;";

			test_sort(arrays, sort_result, intro_sort);
			const auto intro_sort_time = sort_result.duration.count() / sort_result.arrays.size();
			std::cout << std::setw(12) << intro_sort_time << " s;";

			test_sort(arrays, sort_result, merge_sort);
			const auto merge_sort_time = sort_result.duration.count() / sort_result.arrays.size();
			std::cout << std::setw(12) << merge_sort_time << " s;\n";

			file << size << ";" << quick_sort_time << "; " << intro_sort_time << "; " << merge_sort_time << "\n";
		}

		file << "\n";
		file.flush();
		std::cout << "\n";
	}
	file.close();
	std::cout << "Testing finished!\n";
	system("results.csv");
	return 1;
}


