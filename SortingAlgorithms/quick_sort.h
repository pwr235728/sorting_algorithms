﻿#pragma once

#include <vector>
#include "sort.h"

template<class T>
class quick_sort : public sort_algorithm<T>
{
public:
	virtual std::vector<T> sort(const std::vector<T>& data, sorting_order order) override
	{
		std::vector<T> tmp_data = data;
		sort(tmp_data,0, data.size()-1, order);
		return tmp_data;
	}

private:
	static bool compare(T left, T right, sorting_order order)
	{
		if (order == Ascending)
			return left < right;
		else
			return left > right;
	}

	static void sort(std::vector<T>& data, int begin_index, int end_index, sorting_order order)
	{
		unsigned int size = data.size();

		if (data.size() <= 1) return;

		unsigned int middle = (begin_index + end_index) / 2;

		T pivot = data[middle];
		data[middle] = data[end_index];

		int i = begin_index;
		int j = begin_index;

		for (; i < end_index; i++)
		{
			if (compare(data[i], pivot, order))
			{
				std::swap(data[j], data[i]);
				j++;
			}
		}
		data[end_index] = data[j];
		data[j] = pivot;

		if (begin_index < j - 1)
		{
			sort(data, begin_index, j - 1, order);
		}
		if (j + 1 < end_index)
		{
			sort(data, j + 1, end_index, order);
		}
	}
};
