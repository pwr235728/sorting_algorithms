﻿#pragma once

#include <vector>
#include <chrono>
#include "sort.h"

struct test_result
{
	std::vector<std::vector<int>> arrays;
	std::chrono::duration<double> duration{}; // seconds
};

template<class T>
bool is_sorted(const std::vector<T>& ints, sorting_order order = Ascending)
{
	if (ints.size() <= 1)
		return true;

	for (unsigned int i = 1; i<ints.size(); i++)
	{
		if (((order == Ascending) && (ints[i] < ints[i - 1])) ||
			((order == Descending) && (ints[i] > ints[i - 1])))
		{
			return false;
		}
	}
	return true;
}


std::vector<int> get_int_range(int begin, int end, sorting_order order);
void sort_partial(int begin, std::vector<int>& data);
std::vector<int> generate_test_array(int size, double partially);
test_result measure_sort(const std::vector<std::vector<int>>& arrays, sort_algorithm<int>& algorithm);
void test_sort(const std::vector<std::vector<int>>& arrays, test_result& sort_result, sort_algorithm<int>& algorithm);
