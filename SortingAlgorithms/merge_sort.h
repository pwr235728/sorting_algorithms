﻿#pragma once

#include <vector>
#include <algorithm>
#include "sort.h"

template<class T>
class merge_sort : public sort_algorithm<T>
{
public:	
	virtual std::vector<T> sort(const std::vector<T>& data, const sorting_order order) override
	{
		const unsigned int size = data.size();
		std::vector<T> tmp_data = data;

		for(unsigned int run_width = 1; run_width < size; run_width = 2*run_width)
		{
			for(unsigned int i = 0; i < size; i = i + 2*run_width)
			{
				std::vector<T> left_run = std::vector<T>(tmp_data.begin() + std::min(i , size), 
														tmp_data.begin() + std::min(i + run_width, size));

				std::vector<T> right_run = std::vector<T>(tmp_data.begin() + std::min(i + run_width, size), 
														tmp_data.begin() + std::min(i + 2 * run_width, size));

				merge(left_run, right_run, order, tmp_data, std::min(i, size));				
			}
		}
		return tmp_data;
	}
	
private:
	static bool compare(T left, T right, const sorting_order order)
	{
		if (order == Ascending)
			return left <= right;
		else
			return left >= right;
	}

	static void merge(const std::vector<T>& left_run, const std::vector<T>& right_run, const sorting_order order, std::vector<T>& out, const int out_begin)
	{
		unsigned int size = left_run.size() + right_run.size();

		//std::vector<T> merged(size);
		unsigned int left_run_index = 0;
		unsigned int right_run_index = 0;
		unsigned int merged_index = out_begin;

		while (left_run_index < left_run.size() && right_run_index < right_run.size())
		{
			if (compare(left_run[left_run_index], right_run[right_run_index], order))
				out[merged_index++] = left_run[left_run_index++];
			else
				out[merged_index++] = right_run[right_run_index++];
		}
		while (left_run_index < left_run.size())
			out[merged_index++] = left_run[left_run_index++];
		while (right_run_index < right_run.size())
			out[merged_index++] = right_run[right_run_index++];
	}
};
