﻿#pragma once
enum sorting_order
{
	Ascending,
	Descending
};

template<class T>
class sort_algorithm
{
public:
	virtual ~sort_algorithm() = default;
	virtual std::vector<T> sort(const std::vector<T>& data, sorting_order order = Ascending) = 0;
};