﻿#include "algorithms_testing.h"
#include <iostream>
#include <chrono>
#include <random>
#include "quick_sort.h"

std::vector<int> get_int_range(int begin, int end, sorting_order order)
{
	if (end < begin) return {};

	const size_t size = 1 + end - begin;

	std::vector<int> ints(size);

	for (int i = begin; i <= end; i++)
	{
		if (order == Ascending)
			ints[i - begin] = i;
		else
			ints[i - begin] = end - i + 1;
	}

	return ints;
}

void sort_partial(int begin, std::vector<int>& data)
{
	quick_sort<int> sort{};
	std::vector<int> new_data = sort.sort(std::vector<int>(data.begin(), data.begin() + begin), Ascending);
	new_data.insert(new_data.end(), data.begin() + begin, data.end());
	data = new_data;
}

std::vector<int> generate_test_array(int size, double partially)
{
	std::vector<int> ints;

	if (partially < -0.1)
	{
		ints = get_int_range(1, size, Descending);
		partially = -partially;
	}
	else
		ints = get_int_range(1, size, Ascending);

	const int end_index = static_cast<int>(std::round(size * partially));

	if (end_index >= size) return ints;
	if (end_index < size)
	{
		std::shuffle(ints.begin() + end_index, ints.end(), std::mt19937(std::random_device{}()));
		//sort_partial(partially, ints);
	}
	return ints;
}

test_result measure_sort(const std::vector<std::vector<int>>& arrays, sort_algorithm<int>& algorithm)
{
	const auto count = arrays.size();

	test_result result;

	result.arrays.assign(count, {});

	const auto begin = std::chrono::high_resolution_clock::now();
	for (unsigned int i = 0; i < count; i++)
	{
		result.arrays[i] = algorithm.sort(arrays[i], Ascending);
	}
	const auto end = std::chrono::high_resolution_clock::now();

	const auto duration = std::chrono::duration<double>(end - begin);
	result.duration = duration;
	return result;
}

void test_sort(const std::vector<std::vector<int>>& arrays, test_result& sort_result, sort_algorithm<int>& algorithm)
{
	sort_result = measure_sort(arrays, algorithm);
	for (auto& sorted_array : sort_result.arrays)
	{
		if (!is_sorted(sorted_array))
		{
			std::cerr << "Sorting error!\n";
			throw std::exception("Sorting error!\n");
		}
	}
}
