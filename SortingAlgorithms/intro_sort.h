﻿#pragma once

#include <vector>
#include "sort.h"
#include "heap_sort.h"


template<class T>
class intro_sort: public sort_algorithm<T>
{
public:
	virtual std::vector<T> sort(const std::vector<T>& data, sorting_order order) override
	{
		int maxdepth = 2 * std::log2(data.size());
		
		std::vector<T> tmp_data = data;
		sort(tmp_data, 0, data.size()-1 , maxdepth, order);

		return tmp_data;
	}

private:
	static bool compare(T left, T right, sorting_order order)
	{
		if (order == Ascending)
			return left < right;
		else
			return left > right;
	}

	static void sort(std::vector<T>& data, int begin_index, int end_index, int maxdepth, sorting_order order)
	{
		unsigned int size = end_index - begin_index + 1; 
		if (size <= 1)
			return;

		int pivot_index = partition(data, begin_index, end_index, order);
		
		if (size > maxdepth) {
			data = heap_sort<T>::sort(data, order);
		}else {
			sort(data, begin_index, pivot_index, maxdepth - 1, order);
			sort(data, pivot_index + 1, size-1, maxdepth - 1, order);
		}
	}
	static int partition(std::vector<T>& data, int begin_index, int end_index, sorting_order order)
	{
		unsigned int middle = (begin_index + end_index) / 2;

		T pivot = data[middle];
		data[middle] = data[end_index];

		int i = begin_index;
		int j = begin_index;

		for (; i < end_index; i++)
		{
			if (compare(data[i], pivot, order))
			{
				std::swap(data[j], data[i]);
				j++;
			}
		}
		data[end_index] = data[j];
		data[j] = pivot;

		return j;
	}
};

